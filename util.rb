def install_packages(packages, os='ubuntu')
    packages_list = ''
    packages.each { |item| packages_list = packages_list + " '#{item}' " }
    case os
        when "ubuntu"
            return %Q[apt-get update && \
apt-get install -y --no-install-recommends #{packages_list} && \
apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*]
        when "centos"
            return %Q[yum install -y '#{packages_list}' && yum clean all]
    end
end

def deb_file_install(path, with_download=false)
    command = ''
    if with_download
        command = command + "wget #{path} -O temp.deb && "
        path = "temp.deb"
    end
    command = command + %Q[apt-get update && \
dpkg --force-depends -i '#{path}' && \
apt-get -y install -f --no-install-recommends && \
apt-get clean && \
rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* '#{path}']
    return command
end


def configure_ssh_for_devs()
    return %Q[ssh-keygen -A && \
mkdir /var/run/sshd && \
chmod 0755 /var/run/sshd && \
echo 'root:toor' | chpasswd && \
sed -i 's/PermitRootLogin without-password/PermitRootLogin yes/' /etc/ssh/sshd_config && \
sed -i -e 's/#*Port 22/Port 2222/' /etc/ssh/sshd_config && \
sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config && \
sed -i 's/StrictModes yes/#StrictModes yes/' /etc/ssh/sshd_config && \
sed -i '/AcceptEnv */d' /etc/ssh/sshd_config && \
sed 's@session\\s*required\\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd && \
echo "export VISIBLE=now" >> /etc/profile]
end

def configure_neovim()
    return %Q[mkdir -p ~/.config/nvim/autoload && \
echo #{read("/opt/advancedbox/neovim/init.vim")} > ~/.config/nvim/init.vim && \
curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim && \
nvim +PlugInstall +qall --headless]
end

def update_locale(os='ubuntu')
    case os
        when "ubuntu"
            env DEBIAN_FRONTEND: "noninteractive"
            run %Q[#{install_packages("locales")} && \
sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
dpkg-reconfigure --frontend=noninteractive locales && \
update-locale LANG=en_US.UTF-8]
            env LANG: "en_US.UTF-8", LC_ALL: "en_US.UTF-8"
    end
end

def install_wkhtmltopdf(os="ubuntu")
    command = ''
    case os
        when "ubuntu"
            command = %Q[wget https://dl.bintray.com/siredvin/OldStore/pool/w/wkhtmltox/wkhtmltox-0.12.2.1_linux-trusty-amd64.deb -O wkhtmltox.deb && \
#{deb_file_install("wkhtmltox.deb")}]
    end
end