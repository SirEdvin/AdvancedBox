set nowrap
set hidden
set shell=/bin/bash
set encoding=utf-8
set noswapfile
set ttimeout
set ttimeoutlen=10
set laststatus=2
set backspace=indent,eol,start
set shiftwidth=4
set tabstop=4
set softtabstop=4
set smarttab
set expandtab
set nolinebreak
set number
set numberwidth=5
set foldmethod=indent
set foldlevel=999
set completeopt=menu,menuone,longest,noinsert,noselect
set pumheight=7
set splitbelow
set nocompatible

call plug#begin()

Plug 'scrooloose/nerdtree'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'mhartington/oceanic-next'
Plug 'majutsushi/tagbar'

call plug#end()

let \$NVIM_TUI_ENABLE_TRUE_COLOR=1
let g:airline_powerline_fonts = 1
set termguicolors
syntax enable
colorscheme OceanicNext
let g:airline_theme='oceanicnext'
call plug#end()